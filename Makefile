all: ubuntu

provision:
	./install.sh bash git i3-gaps kitty neovim polybar powerline-shell tmux xinitrc xresources

ubuntu:
	./install.sh bash git most neovim
