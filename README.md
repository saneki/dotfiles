dotfiles
========

Configuration files and stuff.

The `packages` directory contains subdirectories (packages) which can be
installed using the `install.sh` script, which uses `stow`.

To install a package to your home directory, use: `./install.sh -S <pkgname>`.
