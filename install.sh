#!/usr/bin/env bash
[ $# -gt 0 ] || { echo "usage: ./install [-D|-R|-S] <packages>"; exit; }

flag='S'

while getopts ':DRS' opt; do
	case $opt in
		D) flag='D' ;;
		R) flag='R' ;;
		S) flag='S' ;;
		\?) ;;
	esac
done

shift $((OPTIND -1))

stow --no-folding -d 'packages' -t ~ "-${flag}" $@
