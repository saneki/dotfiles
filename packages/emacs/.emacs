; Setup package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)

; Setup evil + org modes
(require 'evil-leader)
(global-evil-leader-mode)
(require 'evil)
(evil-mode 1)
(require 'evil-org)

; notmuch mail client
(autoload 'notmuch "notmuch" "notmuch mail" t)

; Disable menu bar, scroll bar, and tool bar
(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

; Set default of 80 chars per line
(setq-default fill-column 80)
; Display column number
(setq-default column-number-mode t)

; Follow symlinks by default
(setq-default vc-follow-symlinks nil)

; Smooth scrolling
(require 'smooth-scrolling)
(smooth-scrolling-mode 1)

; Default C/C++ style (based on K&R)
(c-add-style "def"
	     '("k&r"
	       (c-basic-offset . 4)
	       (tab-width . 4)
	       (indent-tabs-mode . t)))
(setq c-default-style "def")

; Shell mode hook, use tabs
(add-hook 'sh-mode-hook
	  (lambda ()
	    (setq indent-tabs-mode t)
	    (setq tab-width 4)))

; Emacs seems to use this for package synchronization for multiple machines
; using the same config file
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (smooth-scrolling json-mode markdown-mode evil-org evil-leader evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
