#!/bin/sh
# This script is a slightly modified version of:
# https://gist.github.com/mxgrn/663933/2c6944702cb4974cae4e91d40fc066b5b58fe467
#
# This will abort "git commit" and remove the trailing whitespaces/tabs from the files to be committed.
# Simply repeating the last "git commit" command will do the commit then.
#
# Put this into .git/hooks/pre-commit, and chmod +x it.
if git rev-parse --verify HEAD >/dev/null 2>&1
then
  against=HEAD
else
  echo "error: unable to get commit hash of HEAD" 2>&1
  exit 1
fi

if test "$(git diff-index --check --cached $against --)"
then
  echo "COMMIT ABORTED! Removing trailing whitespaces..."
  for FILE in `git diff-index --check --cached $against -- | sed '/^[+-]/d' | cut -d: -f1 | uniq`; do echo "* $FILE" ; sed -i 's/[ \t]*$//' "$FILE" ; done
  echo "Done! Run git commit once again."
  exit 1
fi
