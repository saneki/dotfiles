#!/usr/bin/env bash

# Configure powerline-shell

function _update_ps1() {
	PS1=$(powerline-shell $?)
}

is_executable 'powerline-shell' && {
	if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
		PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
	fi
}
