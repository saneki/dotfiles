#!/usr/bin/env bash

# Configure powerline-go

# Command line options for powerline-go
options="-max-width 100 -newline"

# If themefile is a file, append theme to options
themefile="$XDG_CONFIG_HOME/powerline-go/theme.json"
[ -f $themefile ] && options="$options -theme $themefile"

function _update_ps1() {
	PS1=$(powerline-go -error $? $options)
}

is_executable 'powerline-go' && {
	if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
		PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
	fi
}
