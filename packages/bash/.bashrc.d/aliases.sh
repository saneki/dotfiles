# Simple 1-char aliases
is_executable 'git' && alias g='git'
is_executable 'ls' && alias l='ls -al --color=auto'
is_executable 'math.sh' && alias m='math.sh'
is_executable 'nvim' && alias n='nvim'
is_executable 'tree' && alias t='tree'

# Alias for removing trailing whitespace
alias chomp="sed -E -i 's/[[:space:]]+$//'"

# Colorful ls
alias ls='ls --color=auto'

# Quick clipboard alias
alias xc='xclip -selection clipboard'

# Use neovim aliases if found, otherwise vim
if type nvim 1>/dev/null 2>&1 ; then
	alias v='nvim'
	alias vim='nvim'
else
	alias v='vim'
fi
