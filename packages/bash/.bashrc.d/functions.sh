#!/usr/bin/env bash

# Appends directory "$dir" to PATH (if "$dir" exists as a directory).
add_dir_to_path()
{
	[ $# -eq 1 ] || return 1

	dir="$1"
	[ -d "$dir" ] && PATH="$PATH:$dir"
}

# Checks if a name is an executable.
is_executable()
{
	[ $# -eq 1 ] || return 1

	which "$1" 1>/dev/null 2>/dev/null
}

# Makes a directory "$dir" and changes to it.
mkdircd()
{
	[ $# -eq 1 ] || return 1

	dir="$1"
	mkdir -p "$dir" && cd "$dir"
}

# Source a file if it exists.
source_if_exists()
{
	[ $# -eq 1 ] || return 1

	file="$1"
	[ -f "$file" ] && . "$file"
}

# Initialize an SSH agent session.
ssh-agent-init()
{
	eval "$(ssh-agent -s)"
	ssh-add ~/.ssh/id_ed25519
}
