#!/usr/bin/env bash

# Explicit definitions for XDG environment variable paths.
# See: https://wiki.archlinux.org/index.php/XDG_Base_Directory

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
