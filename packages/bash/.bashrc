#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Default shell prompt
PS1='[\u@\h \W]\$ '

# Common exports
export EDITOR="nvim"
export TERMINAL="kitty"

# Options for less:
# -i: Ignore case when searching.
# -q: Do not ring bell if attempting scroll past end of contents.
# -X: Don't clear screen on exit.
# -F: Exit if text is less than one screen long.
# -R: Ensure ANSI color escape sequences are output "raw."
export LESS='-iqXFR'

# Include "hidden" files when globbing
shopt -s dotglob

# Source personal functions file if it exists
if [ -f "$HOME/.bashrc.d/functions.sh" ]; then
  . "$HOME/.bashrc.d/functions.sh"
fi

# If debian system, use default PS1 from Ubuntu's bashrc script.
if [ -r '/etc/debian_version' ]; then
  source_if_exists "$HOME/.bashrc.d/debian-colors.sh"
fi

# If "most" is installed, use as default pager for man.
is_executable 'most' && export MANPAGER='most'

# Source script with XDG environment variables if it exists
source_if_exists "$HOME/.bashrc.d/xdg.sh"

# Source lesspipe config
source_if_exists "$HOME/.bashrc.d/lesspipe.sh"

# Custom scripts
add_dir_to_path "$HOME/.bin"

# cargo (rust) packages
add_dir_to_path "$HOME/.cargo/bin"

# pip (python) packages
add_dir_to_path "$HOME/.local/bin"

# Dart (pub) packages
add_dir_to_path "$HOME/.pub-cache/bin"

# Source personal aliases file if it exists
source_if_exists "$HOME/.bashrc.d/aliases.sh"

source_if_exists "$HOME/.bashrc.d/powerline-go.sh"

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
