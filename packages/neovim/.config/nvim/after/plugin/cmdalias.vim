if exists(':Alias')
	" Aliases for common operations (accidental shift / caps lock)
	Alias Q  q
	Alias W  w
	Alias WQ wq

	" suda.vim aliases
	Alias e!!      e\ suda://%
	Alias r!!      r\ suda://%
	Alias w!!      w\ suda://%
	Alias saveas!! saveas\ suda://%
endif
