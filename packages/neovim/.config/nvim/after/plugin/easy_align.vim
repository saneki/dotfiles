" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" Align GitHub-flavored Markdown tables
" https://robots.thoughtbot.com/align-github-flavored-markdown-tables-in-vim
au FileType markdown xmap <Leader><Bslash> :EasyAlign*<Bar><Enter>
