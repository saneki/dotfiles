" Hack for Windows + Git Bash:
" vim-airline has issue with quoted full shell path for Git Bash.
"  Commit:   0ed7b8bb2ed68d65a5ba55317896680b31d315b0
"  Function: airline#async#nvim_vcs_clean:25
if &shell =~ 'bash.exe'
  " Prevent using full path, use proper shellcmdflag.
  let &shell = 'bash.exe'
  let &shellcmdflag = '-c'
endif

let mapleader="\<Space>"

call plug#begin()

" EditorConfig support for vim
Plug 'sgur/vim-editorconfig'

" Git gutter
Plug 'airblade/vim-gitgutter'

" TOML syntax support
Plug 'cespare/vim-toml'

" JSON
Plug 'elzr/vim-json'

" Org-Mode for vim
Plug 'jceb/vim-orgmode'

" Alignment plugin
Plug 'junegunn/vim-easy-align'

" Command aliases
Plug 'Konfekt/vim-alias'

" An alternative sudo.vim for Vim and Neovim
Plug 'lambdalisue/suda.vim'

" Easily search for, substitute, and abbreviate multiple variants of a word
Plug 'tpope/vim-abolish'

" Comment stuff out
Plug 'tpope/vim-commentary'

" Helpers for UNIX
Plug 'tpope/vim-eunuch'

" Git wrapper
Plug 'tpope/vim-fugitive'

" Sensible defaults
Plug 'tpope/vim-sensible'

" Quoting / Parenthesizing made simple
Plug 'tpope/vim-surround'

" Pairs of handy bracket mappings
Plug 'tpope/vim-unimpaired'

" Gotham colorscheme
Plug 'whatyouhide/vim-gotham'

" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

" Tab config (appear as 4 spaces)
set tabstop=4
set softtabstop=0 noexpandtab
set shiftwidth=4

" Text width (line length)
set textwidth=100

" Map ; to :
nnoremap ; :
vnoremap ; :

" Escape terminal mode with Esc
tnoremap <Esc> <C-\><C-n>

" 2-space indented YAML (for ansible)
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Colors
silent! colorscheme gotham

" Use exact colors in terminal if possible
set termguicolors

" Shorter update time for vim-gitgutter
set updatetime=100
