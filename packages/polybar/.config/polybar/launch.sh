#!/bin/bash

# Partly borrowed from: https://forum.archlabslinux.com/t/polybar-on-dual-monitors/2545/2

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
polybar Work-DP1 &
polybar Work-DP2 &

echo "Polybar launched..."
