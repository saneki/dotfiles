#!/usr/bin/env bash

fail() {
	echo '?'
	exit 0
}

type curl 1>/dev/null 2>&1 || fail
type jq 1>/dev/null 2>&1 || fail

[ -z "$REDDIT_INBOX_FEED" ] && fail
[ -z "$REDDIT_INBOX_USER" ] && fail

agent='Polybar Script'
url="https://www.reddit.com/message/unread/.json?feed=${REDDIT_INBOX_FEED}&user=${REDDIT_INBOX_USER}"

unread="$(curl -s -A "$agent" "$url" | jq '.["data"]["children"] | length' 2>/dev/null)"
[ ! -z "$unread" ] && echo "$unread" || fail
